//
//  RecipeTableViewCell.swift
//  appapp
//
//  Created by ertuğrul gazi akça on 29.07.2018.
//  Copyright © 2018 Bahadır Talha Çakıcı. All rights reserved.
//

import UIKit

class RecipeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var recipeName: UILabel!
    
    @IBOutlet weak var recipeImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
