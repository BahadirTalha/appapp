//
//  RecipesTableViewController.swift
//  appapp
//
//  Created by ertuğrul gazi akça on 29.07.2018.
//  Copyright © 2018 Bahadır Talha Çakıcı. All rights reserved.
//

import UIKit

class RecipesTableViewController: UITableViewController {
    
    let recipeNames = ["Dolma","Mantı","Cacık","Alinazik","Mercimek Köfte","Barbunya Pilaki","Patates Kızartma","Nohut","İzmir Köfte","Islama Köfte"]
    let recipeImages = [#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik"),#imageLiteral(resourceName: "alinazik")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.tableFooterView = UIView()
        
        //JSON verilerinin bulunduğu adres. Veriler bu url den parse edilecek. --> TÜRKÇE KARAKTER SIKINTISI VAR SANKİ !!!
        //http://ertugrulgaziakca.com/recipes.json
    

    }

    @IBAction func showMainPage(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return recipeNames.count //tarif sayısı kadar cell dönmesini istiyoruz
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath) as! RecipeTableViewCell

        // Configure the cell...
        let rowNumber = indexPath.row
        cell.recipeName.text = recipeNames[rowNumber]
        cell.recipeImage.image = recipeImages[rowNumber]
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowNumber = indexPath.row
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReadRecipeStoryboardID") as! ReadRecipeViewController
        
        vc.selectedRow = rowNumber
        
        vc.title = recipeNames[rowNumber]
        
        self.show(vc, sender: nil)
    }

 

}
