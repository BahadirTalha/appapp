//
//  ReadRecipeViewController.swift
//  appapp
//
//  Created by ertuğrul gazi akça on 1.08.2018.
//  Copyright © 2018 Bahadır Talha Çakıcı. All rights reserved.
//

import UIKit

class ReadRecipeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedRow = 0;

    
    var menuImages = [UIImage(named: "dolma"),
                      UIImage(named: "manti"),
                      UIImage(named: "cacik"),
                      UIImage(named: "alinazik"),
                      UIImage(named: "manti"),
                      UIImage(named: "cacik"),
                      UIImage(named: "manti"),
                      UIImage(named: "cacik"),
                      UIImage(named: "manti"),
                      UIImage(named: "cacik")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 2200
    }
    
    
   
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeImageCell", for: indexPath) as! RecipeImageTableViewCell
            
            cell.recipeImage.image = menuImages[selectedRow]

            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReadRecipeCell", for: indexPath) as! ReadRecipeTableViewCell
            //Tarifi Nasıl yapıldığı konacak
            
            return cell

        }

    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    

   

}
