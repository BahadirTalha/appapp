//
//  MaterialsTableViewController.swift
//  appapp
//
//  Created by ertuğrul gazi akça on 9.08.2018.
//  Copyright © 2018 Bahadır Talha Çakıcı. All rights reserved.
//

import UIKit

class MaterialsTableViewController: UITableViewController {

    let materials = ["Tuz","Limon","Patates","Su","Un"]

    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return materials.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MaterialsCell", for: indexPath) as! MaterialsTableViewCell

        let rowNumber = indexPath.row
        
        cell.materialName.text = materials[rowNumber]
        
        // Configure the cell...

        return cell
    }
 

  

}
