//
//  ReadRecipeTableViewCell.swift
//  appapp
//
//  Created by ertuğrul gazi akça on 1.08.2018.
//  Copyright © 2018 Bahadır Talha Çakıcı. All rights reserved.
//

import UIKit

class ReadRecipeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var readRecipeLabel: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
